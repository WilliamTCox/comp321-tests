module Utilities
  def self.index_from_ordinal(ordinal)
    unless @index_from_ordinal
      @index_from_ordinal = %i[first second].
          each_with_index.each_with_object({}) { |(ordinal, index), hash| hash[ordinal] = index }
      @index_from_ordinal[:last] = -1
    end
    @index_from_ordinal[ordinal.downcase.to_sym]
  end
end
