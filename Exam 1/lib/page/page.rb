require 'uri'
require 'rspec/expectations'
require 'json'

RSpec::Matchers.define :have_proper_page_layout do
  match do |page|
    @expected_layout = page.class.expected_layout
    @actual_layout   = page.actual_layout.select { |key, value| @expected_layout.include?(key) }
    @actual_layout == @expected_layout
  end

  failure_message do |page|
    "expected that page would be properly laid out for a #{page.class.page_type} page." \
      "\nExpected:\n#{@expected_layout}" \
      "\nGot:\n#{@actual_layout}"
  end
end

module Page
  class Page
    include RSpec::Matchers

    SITE_URI = URI.parse(ENV['SITE_URI'])

    def initialize(browser)
      @browser = browser
    end

    def visit
      @browser.goto(page_uri.to_s)
    end

    def site_uri
      SITE_URI
    end

    def title
      @browser.title
    end

    def heading
      @browser.h1
    end

    def alert_text
      @browser.div(id: 'alert').text
    end

    def actual_layout
      {
          title:   title,
          heading: (heading.text if heading.exists?)
      }.reject { |key, value| value.nil? || value.to_s.empty? }
    end

    def self.expected_layout
      TestData.page_layouts[page_type]
    end

    def self.page_type
      @test ||= self.to_s.split('::').last.to_sym
    end

    def button_element_by_value(value)
      button = @browser.button(value: value)
      expect(button).to exist
      button
    end
  end
end
