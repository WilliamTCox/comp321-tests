require_relative 'page'

RSpec::Matchers.define :display_product do |expected_product|
  match do |page|
    @actual_product = page.displayed_product
    @actual_product == expected_product
  end

  failure_message do |page|
    "expected that page would display the right product information." \
      "\nExpected:\n#{expected_product}" \
      "\nGot:\n#{@actual_product}"
  end
end

module Page
  class Product < Page
    INPUT_NAME = {
        code:        'productCode',
        description: 'productDescription',
        price:       'productPrice'
    }

    def initialize(browser)
      super(browser)
    end

    def text_field_by_name(name)
      field = @browser.text_field(name: INPUT_NAME[name])
      expect(field).to exist
      field
    end

    def code_element
      text_field_by_name(:code)
    end

    def code
      text_field_by_name(:code).value
    end

    def description
      text_field_by_name(:description).value
    end

    def price
      text_field_by_name(:price).value
    end

    def code=(value)
      field = text_field_by_name(:code)
      field.set(value) unless field.readonly?
    end

    def description=(value)
      text_field_by_name(:description).set(value)
    end

    def price=(value)
      text_field_by_name(:price).set(value)
    end

    def displayed_product=(p)
      self.code        = p.code
      self.description = p.description
      self.price       = p.price
    end

    def displayed_product
      ::Product.new(code, description, price)
    end

    def view_products
      button_element_by_value('View Products')
    end

    def update_product
      button_element_by_value('Update Product')
    end
  end
end
