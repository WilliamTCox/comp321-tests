require_relative 'page'

module Page
  class Products < Page
    PATH = '/productMaint_cox99/displayProducts'

    TABLE_INDEX = %i[code description price edit delete].
        each_with_index.each_with_object({}) { |(label, index), hash| hash[label] = index }

    LINK_TEXT = {
        edit:   'Edit',
        delete: 'Delete'
    }

    def initialize(browser)
      super(browser)
    end

    def headers
      text_from_row(product_table.headers)
    end

    def page_uri
      site_uri + PATH
    end

    def product_table
      @browser.table
    end

    def product_hashes
      product_table.hashes
    end

    def products
      product_hashes.collect { |watir_hash| ::Product.from_watir_hash(watir_hash) }
    end

    def product_rows
      product_table.rows[1..-1]
    end

    def product_row(index)
      index += 1 unless index < 0
      product_table[index]
    end

    def product(index)
      values     = text_from_row(product_row(index))
      watir_hash = Hash[headers.zip(values)]

      ::Product.from_watir_hash(watir_hash)
    end

    def edit_product_elements
      product_rows.collect { |product_row| link_from_row(product_row, :edit) }
    end

    def delete_product_elements
      product_rows.collect { |product_row| link_from_row(product_row, :delete) }
    end

    def product_element_by_type(index, type)
      element = link_from_row(product_row(index), type)
      expect(element).to exist
      element
    end

    def edit_product_element(index)
      product_element_by_type(index, :edit)
    end

    def delete_product_element(index)
      product_element_by_type(index, :delete)
    end

    def add_product_element
      button_element_by_value('Add Product')
    end

    def link_from_row(table_row, link_type)
      table_row[TABLE_INDEX[link_type]].link(text: LINK_TEXT[link_type])
    end

    def text_from_row(table_row)
      table_row.collect { |value| value.text }
    end
  end
end
