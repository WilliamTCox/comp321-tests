require_relative 'page'

module Page
  class ConfirmDelete < Page
    FIELD_NAME = {
        code:        'productCode',
        description: 'productDescription',
        price:       'productPrice'
    }

    def initialize(browser)
      super(browser)
    end

    def value_by_field(field)
      element = @browser.span(id: FIELD_NAME[field])
      expect(element).to exist
      element.text
    end

    def code
      value_by_field(:code)
    end

    def description
      value_by_field(:description)
    end

    def price
      value_by_field(:price)
    end

    def displayed_product
      ::Product.new(code, description, price.gsub(/^\$/, ''))
    end

    def yes_element
      button_element_by_value('Yes')
    end

    def no_element
      button_element_by_value('No')
    end
  end
end
