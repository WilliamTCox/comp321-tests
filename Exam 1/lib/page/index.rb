require_relative 'page'

module Page
  class Index < Page
    PATH = '/productMaint_cox99'

    def initialize(browser)
      super(browser)
    end

    def page_uri
      site_uri + PATH
    end

    def view_products
      link = @browser.link(text: 'View Products')
      expect(link).to exist
      link
    end
  end
end
