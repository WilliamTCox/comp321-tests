require './lib/page/confirm_delete'
require './lib/page/index'
require './lib/page/product'
require './lib/page/products'

module PageHelper
  def self.page_class_by_name(page_name)
    case page_name
      when 'index'
        Page::Index
      when 'Products'
        Page::Products
      when 'Product'
        Page::Product
      when 'Confirm Delete'
        Page::ConfirmDelete
      else
        fail "Unrecognized page: #{page}"
    end
  end

  def self.page_by_name(page_name, browser)
    page_class_by_name(page_name).new(browser)
  end
end
