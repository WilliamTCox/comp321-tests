module Messages
  def self.product_added(product)
    "Added the product with code \"#{product.code}\""
  end

  def self.product_updated(product)
    "Updated the product with code \"#{product.code}\""
  end

  def self.product_deleted(product)
    "Deleted the product with code \"#{product.code}\""
  end

  def self.product_code_exists(product)
    "The product code \"#{product.code}\" already exists. Please enter a new product code."
  end

  def self.product_code_invalid(product)
    "The product code \"#{product.code}\" is invalid. Please enter a valid, alphanumeric code."
  end

  def self.product_price_invalid(product)
    "The product price \"#{product.price}\" is invalid. Please enter a valid price."
  end

  def self.product_price_too_high(product)
    "The product price \"#{product.price}\" is too high. Please enter a price less than 100000.00."
  end

  def self.product_code_missing
    "Product code was empty. Please enter a product code."
  end

  def self.product_description_missing
    "Product description was empty. Please enter a product description."
  end

  def self.product_price_missing
    "Product price was empty. Please enter a product price."
  end
end
