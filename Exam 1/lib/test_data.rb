require 'csv'
require 'json'
require 'yaml'

module TestData
  NEW_PRODUCTS_FILENAME = './test_data/new_products.txt'
  PRODUCTS_FILENAME     = './test_data/products.txt'

  PAGE_LAYOUT_FILENAME = './test_data/page_layouts.json'

  MYSQL_USER_FILENAME         = './test_data/mysql_user.json'
  RESET_DATABASE_SQL_FILENAME = './test_data/reset_database.sql'

  def self.products
    @products ||= read_products(PRODUCTS_FILENAME)
  end

  def self.new_products
    @new_products ||= read_products(NEW_PRODUCTS_FILENAME)
  end

  def self.page_layouts
    @page_layouts ||= JSON.parse(File.read(PAGE_LAYOUT_FILENAME), symbolize_names: true)
  end

  def self.mysql_user
    @mysql_user ||= JSON.parse(File.read(MYSQL_USER_FILENAME), symbolize_names: true)
  end

  def self.reset_database_sql
    @reset_database_sql ||= File.read(RESET_DATABASE_SQL_FILENAME)
  end

  def self.read_products(filename)
    CSV.
        read(filename, col_sep: '|').
        collect { |product| Product.from_csv_array(product) }
  end
end
