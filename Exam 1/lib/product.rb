class Product
  attr_accessor :code
  attr_accessor :description
  attr_accessor :price

  def initialize(code, description, price)
    @code        = code
    @description = description
    @price       = price
  end

  def ==(other)
    @code == other.code &&
        @description == other.description &&
        @price == other.price
  end

  def to_h
    {
        code:        @code,
        description: @description,
        price:       @price
    }
  end

  def to_s
    "Product:#{to_h}"
  end

  def self.from_csv_array(csv_array)
    self.new(*csv_array)
  end

  def self.from_watir_hash(watir_hash)
    self.new(watir_hash['Code'], watir_hash['Description'], watir_hash['Price'].gsub(/^\$/, ''))
  end

  def self.empty_product
    self.new('', '', '')
  end
end
