And(/^the user has changed the description and price of the product$/) do
  new_data = TestData.new_products.first.dup

  @product_new             = @product_old.dup
  @product_new.description = new_data.description
  @product_new.price       = new_data.price

  @page.description = @product_new.description
  @page.price       = @product_new.price
end

And(/^the user has (?:entered|updated) all information for (a new|the) product$/) do |new_or_updated|
  @product_new      = TestData.new_products.first.dup

  @product_new.code = @product_old.code unless new_or_updated == 'a new'

  @page.displayed_product = @product_new
end

And(/^the user has entered all information for a new product with an existing product code$/) do
  @product_new = TestData.new_products.first.dup

  @product_new.code = TestData.products.first.code

  @page.displayed_product = @product_new
end

And(/^the user has (?:entered|updated) all information for (a new|the) product with a (non-numeric|negative|high) price$/) do |new_or_updated, price|
  @product_new       = TestData.new_products.first.dup

  @product_new.code  = @product_old.code unless new_or_updated == 'a new'
  @product_new.price = case price
    when 'non-numeric'
      'abc'
    when 'negative'
      '-1.00'
    when 'high'
      '100000.00'
    else
      fail "Unrecognized option: #{price}"
  end

  @page.displayed_product = @product_new
end

And(/^the user has (?:entered|updated) all information for (a new|the) product with a price with fractional cents$/) do |new_or_updated|
  @product_new       = TestData.new_products.first.dup

  @product_new.code  = @product_old.code unless new_or_updated == 'a new'
  @product_new.price = '14.955'

  @page.displayed_product = @product_new
end

And(/^the user has (?:entered|updated) all information for (a new|the) product with a price of zero$/) do |new_or_updated|
  @product_new       = TestData.new_products.first.dup

  @product_new.code  = @product_old.code unless new_or_updated == 'a new'
  @product_new.price = '0.00'

  @page.displayed_product = @product_new
end

And(/^the user has (?:entered|updated) all information for (a new|the) product except the (code|description|price)$/) do |new_or_updated, information|
  @product_new      = TestData.new_products.first.dup

  @product_new.code = @product_old.code unless new_or_updated == 'a new'
  case information
    when 'code'
      @product_new.code = ''
    when 'description'
      @product_new.description = ''
    when 'price'
      @product_new.price = ''
    else
      fail "Unrecognized field: #{information}"
  end

  @page.displayed_product = @product_new
end

And(/^the user tries to (add|update) the product without a description or a price$/) do |add_or_update|
  @product_new             = TestData.new_products.first.dup

  @product_new.code        = @product_old.code unless add_or_update == 'add'
  @product_new.description = ''
  @product_new.price       = ''

  @page.displayed_product = @product_new
  @page.update_product.click
end

And(/^the user fixes the empty description$/) do
  product_new_data = TestData.new_products.first.dup

  @product_new.description = product_new_data.description

  @page.displayed_product = @product_new
end
