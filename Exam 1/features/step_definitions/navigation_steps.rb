Given(/^the user wants to visit the (index|Products) page of the site$/) do |page_name|
  @page = PageHelper::page_by_name(page_name, @browser)
end

When(/^the user visits the page$/) do
  @page.visit
end

Given(/^the user has navigated to the (index|Products) page$/) do |page_name|
  @page = PageHelper::page_by_name(page_name, @browser)
  @page.visit
end

When(/^the user follows the link to view products$/) do
  @page.view_products.click
end

When(/^the user chooses to edit the (first|second|last) product$/) do |choice|
  index        = Utilities.index_from_ordinal(choice)
  @product_old = @page.product(index)
  element      = @page.edit_product_element(index)
  element.click
end

When(/^the user chooses to delete the (first|second|last) product$/) do |choice|
  index        = Utilities.index_from_ordinal(choice)
  @product_old = @page.product(index)
  element      = @page.delete_product_element(index)
  element.click
end

When(/^the user chooses to add a new product$/) do
  element = @page.add_product_element
  element.click
end

Given(/^the user has navigated to the Product page to edit the (first) product$/) do |choice|
  @page = Page::Products.new(@browser)
  @page.visit

  index        = Utilities.index_from_ordinal(choice)
  @product_old = @page.product(index)
  element      = @page.edit_product_element(index)
  element.click

  @page = Page::Product.new(@browser)
end

Given(/^the user has navigated to the Confirm Delete page to delete the (first|second|last) product$/) do |choice|
  @page = Page::Products.new(@browser)
  @page.visit

  index        = Utilities.index_from_ordinal(choice)
  @product_old = @page.product(index)
  element      = @page.delete_product_element(index)
  element.click

  @page = Page::ConfirmDelete.new(@browser)
end

Given(/^the user has navigated to the Product page to add a new product$/) do
  @page = Page::Products.new(@browser)
  @page.visit

  element = @page.add_product_element
  element.click

  @page = Page::Product.new(@browser)
end

When(/^the user chooses to view products instead of (?:updating|adding) the product$/) do
  @page.view_products.click
end

When(/^the user chooses to (?:add|update) the product$/) do
  @page.update_product.click
end

When(/^the user chooses to not delete the product$/) do
  @page.no_element.click
end

When(/^the user chooses to delete the product$/) do
  @page.yes_element.click
end
