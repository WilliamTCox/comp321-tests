Then(/^the (index|Products) page will be displayed$/) do |page_name|
  @page = PageHelper::page_by_name(page_name, @browser)
  expect(@page).to have_proper_page_layout
end

Then(/^the Products page will display a list of the (?:(new|updated) and )?existing products( without the deleted product)?$/) do |new_product,  without_old_product|
  @page = Page::Products.new(@browser)

  actual_products   = @page.products
  expected_products = TestData.products.dup

  expected_products.delete_if { |product| product.code == @product_new.code } if new_product == 'updated'
  expected_products.push(@product_new) if new_product
  expected_products.delete(@product_old) if without_old_product

  expect(actual_products).to match_array(expected_products)
end

Then(/^the Products page will display prices formatted as currency$/) do
  @page = Page::Products.new(@browser)

  products_table = @page.product_hashes
  prices         = products_table.collect { |product| product['Price'] }

  re_currency = /^\$\d{1,3}(?:,\d{3})*\.\d{2}$/

  expect(prices).to all(match(re_currency))
end

Then(/^the Products page will provide the option to edit each product$/) do
  @page    = Page::Products.new(@browser)
  elements = @page.edit_product_elements
  expect(elements).to all(exist)
end

Then(/^the Products page will provide the option to delete each product$/) do
  @page    = Page::Products.new(@browser)
  elements = @page.delete_product_elements
  expect(elements).to all(exist)
end

Then(/^the Products page will provide the option to add a product$/) do
  @page   = Page::Products.new(@browser)
  element = @page.add_product_element
  expect(element).to exist
end

Then(/^the Product page will display the product the user chose$/) do
  @page = Page::Product.new(@browser)
  expect(@page).to have_proper_page_layout
  expect(@page).to display_product(@product_old)
end

Then(/^the Product page will display the (?:new|updated) product information$/) do
  @page = Page::Product.new(@browser)
  expect(@page).to have_proper_page_layout
  expect(@page).to display_product(@product_new)
end

Then(/^the product information will be empty on the Product page$/) do
  @page = Page::Product.new(@browser)
  expect(@page).to have_proper_page_layout
  expect(@page).to display_product(Product.empty_product)
end

Then(/^the Confirm Delete page will display the product the user chose$/) do
  @page = Page::ConfirmDelete.new(@browser)
  expect(@page).to have_proper_page_layout
  expect(@page).to display_product(@product_old)
end

And(/^the product code will be read only on the Product page$/) do
  @page = Page::Product.new(@browser)
  expect(@page.code_element).to be_readonly
end
