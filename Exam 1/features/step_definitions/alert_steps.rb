Then(/^the Products page will display an alert that the product has been added$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_added(@product_new))
end

Then(/^the Products page will display an alert that the product has been updated$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_updated(@product_new))
end

Then(/^the Products page will display an alert that the product has been deleted$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_deleted(@product_old))
end

Then(/^the Product page will display an alert that the product code already exists$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_code_exists(@product_new))
end

Then(/^the Product page will display an alert that the product code is invalid$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_code_invalid(@product_new))
end

Then(/^the Product page will display an alert that the product price is invalid$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_price_invalid(@product_new))
end

Then(/^the Product page will display an alert that the product price is too high$/) do
  @page = Page::Products.new(@browser)
  expect(@page.alert_text).to eq(Messages.product_price_too_high(@product_new))
end

Then(/^the Product page will display an alert that the (code|description|price) is missing$/) do |information|
  @page = Page::Products.new(@browser)

  message = case information
    when 'code'
      Messages.product_code_missing
    when 'description'
      Messages.product_description_missing
    when 'price'
      Messages.product_price_missing
    else
      fail "Unrecognized field: #{information}"
  end

  expect(@page.alert_text).to match(message)
end
