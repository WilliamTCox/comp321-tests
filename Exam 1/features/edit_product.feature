Feature: Edit products from the Product page

  Scenario Outline: Choosing to edit a product will display the product on the Product page
    Given the user has navigated to the Products page
    When the user chooses to edit the <choice> product
    Then the Product page will display the product the user chose
    And the product code will be read only on the Product page
    Examples:
      | choice |
      | first  |
      | second |
      | last   |

  Scenario: The Product page will allow the user to go back to view products without making any edits
    Given the user has navigated to the Product page to edit the first product
    And the user has changed the description and price of the product
    When the user chooses to view products instead of updating the product
    Then the Products page will be displayed
    And the Products page will display a list of the existing products

  Scenario: The Product page will allow the user to edit a product
    Given the user has navigated to the Product page to edit the first product
    And the user has updated all information for the product
    When the user chooses to update the product
    Then the Products page will be displayed
    And the Products page will display an alert that the product has been updated
    And the Products page will display a list of the updated and existing products

  Scenario: The Product page will display an alert if the user edits an invalid price
    Given the user has navigated to the Product page to edit the first product
    And the user has entered all information for the product with a price with fractional cents
    When the user chooses to update the product
    Then the Product page will display the updated product information
    And the Product page will display an alert that the product price is invalid

  Scenario Outline: The Product page will display an alert if the user adds a non-numeric or negative price
    Given the user has navigated to the Product page to edit the first product
    And the user has updated all information for the product with a <price> price
    When the user chooses to update the product
    Then the Product page will display the updated product information
    And the Product page will display an alert that the product price is invalid
    Examples:
      | price       |
      | non-numeric |
      | negative    |

  Scenario: The Product page will display an alert if the user adds a price that is too high
    Given the user has navigated to the Product page to edit the first product
    And the user has updated all information for the product with a high price
    When the user chooses to update the product
    Then the Product page will display the updated product information
    And the Product page will display an alert that the product price is too high

  Scenario: The Product page will allow the user to add a new product with a price of zero
    Given the user has navigated to the Product page to edit the first product
    And the user has updated all information for the product with a price of zero
    When the user chooses to add the product
    Then the Products page will be displayed
    And the Products page will display an alert that the product has been updated
    And the Products page will display a list of the updated and existing products

  Scenario Outline: The Product page will display an alert if the user does not enter some information
    Given the user has navigated to the Product page to edit the first product
    And the user has updated all information for the product except the <information>
    When the user chooses to update the product
    Then the Product page will display the updated product information
    And the Product page will display an alert that the <information> is missing
    Examples:
      | information |
      | description |
      | price       |

  Scenario: When fixing an error for empty description, submitting will display the next error
    Given the user has navigated to the Product page to edit the first product
    And the user tries to update the product without a description or a price
    And the user fixes the empty description
    When the user chooses to update the product
    Then the Product page will display the updated product information
    And the Product page will display an alert that the price is missing

