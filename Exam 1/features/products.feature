Feature: Products page

  Scenario: Products page will show a list of products
    Given the user wants to visit the Products page of the site
    When the user visits the page
    Then the Products page will display a list of the existing products

  Scenario: Products page will show a list of products
    Given the user wants to visit the Products page of the site
    When the user visits the page
    Then the Products page will display prices formatted as currency

  Scenario: Products page will provide the option to edit each product
    Given the user wants to visit the Products page of the site
    When the user visits the page
    Then the Products page will provide the option to edit each product

  Scenario: Products page will provide the option to add a product
    Given the user wants to visit the Products page of the site
    When the user visits the page
    Then the Products page will provide the option to add a product

  Scenario: Products page will provide the option to delete each product
    Given the user wants to visit the Products page of the site
    When the user visits the page
    Then the Products page will provide the option to delete each product
