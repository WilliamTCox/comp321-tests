Feature: Add products from the Product page

  Scenario: Choosing to add a product will display the product on the Product page
    Given the user has navigated to the Products page
    When the user chooses to add a new product
    Then the product information will be empty on the Product page

  Scenario: The Product page will allow the user to go back to view products without adding a product
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product
    When the user chooses to view products instead of adding the product
    Then the Products page will be displayed
    And the Products page will display a list of the existing products

  Scenario: The Product page will allow the user to add a new product
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product
    When the user chooses to add the product
    Then the Products page will be displayed
    And the Products page will display an alert that the product has been added
    And the Products page will display a list of the new and existing products

  Scenario: The Product page will display an alert if the user adds an existing product code
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product with an existing product code
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the product code already exists

  Scenario: The Product page will display an alert if the user adds an invalid price
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product with a price with fractional cents
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the product price is invalid

  Scenario Outline: The Product page will display an alert if the user adds a non-numeric or negative price
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product with a <price> price
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the product price is invalid
    Examples:
      | price       |
      | non-numeric |
      | negative    |

  Scenario: The Product page will display an alert if the user adds a price that is too high
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product with a high price
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the product price is too high

  Scenario: The Product page will allow the user to add a new product with a price of zero
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product with a price of zero
    When the user chooses to add the product
    Then the Products page will be displayed
    And the Products page will display an alert that the product has been added
    And the Products page will display a list of the new and existing products

  Scenario Outline: The Product page will display an alert if the user does not enter some information
    Given the user has navigated to the Product page to add a new product
    And the user has entered all information for a new product except the <information>
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the <information> is missing
    Examples:
      | information |
      | code        |
      | description |
      | price       |

  Scenario: When fixing an error for empty description, submitting will display the next error
    Given the user has navigated to the Product page to add a new product
    And the user tries to add the product without a description or a price
    And the user fixes the empty description
    When the user chooses to add the product
    Then the Product page will display the new product information
    And the Product page will display an alert that the price is missing
