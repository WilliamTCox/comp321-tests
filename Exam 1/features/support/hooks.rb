require 'base64'
require 'fileutils'

require 'mysql2'
require 'watir'

module Hooks
  def self.reload_database
    mysql_user            = TestData.mysql_user.dup
    mysql_user[:password] = Base64.decode64(mysql_user[:password])

    client = Mysql2::Client.new({flags: Mysql2::Client::MULTI_STATEMENTS}.merge(mysql_user))
    client.query(TestData.reset_database_sql)
    client.abandon_results!
    client.close
  end

  def self.browser
    @browser ||= Watir::Browser.new
  end

  def self.close_browser
    @browser.close
    @browser = nil
  end
end

Before do
  @browser = Hooks.browser
  Hooks.reload_database
end

at_exit do
  Hooks.reload_database
end
