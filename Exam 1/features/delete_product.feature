Feature: Delete products from the Product page

  Scenario Outline: Choosing to delete a product will display the product on the Confirm Delete page
    Given the user has navigated to the Products page
    When the user chooses to delete the <choice> product
    Then the Confirm Delete page will display the product the user chose
    Examples:
      | choice |
      | first  |
      | second |
      | last   |

  Scenario: The Confirm Delete page will allow the user to go back to view products without deleting the product
    Given the user has navigated to the Confirm Delete page to delete the first product
    When the user chooses to not delete the product
    Then the Products page will be displayed
    And the Products page will display a list of the existing products

  Scenario Outline: The Confirm Delete page will allow the user to delete the product
    Given the user has navigated to the Confirm Delete page to delete the <choice> product
    When the user chooses to delete the product
    Then the Products page will be displayed
    And the Products page will display an alert that the product has been deleted
    And the Products page will display a list of the existing products without the deleted product
    Examples:
      | choice |
      | first  |
      | second |
      | last   |
