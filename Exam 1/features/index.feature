Feature: Product Maintenance index page

  Scenario: The Product Maintenance index page can be displayed
    Given the user wants to visit the index page of the site
    When the user visits the page
    Then the index page will be displayed

  Scenario: The index page links to the Products page
    Given the user has navigated to the index page
    When the user follows the link to view products
    Then the Products page will be displayed
